package com.ufg.projeto1grupo2;

 
import com.sun.xml.internal.fastinfoset.sax.SystemIdResolver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.String.*;

public class Logic_old {
    private static char[][] matrix;
    private ServerSocket server;
    private boolean endTurn;


    private char generateRandomLetter() {
        /*
        * Generate random letters to fill Matrix
        * */

        return (char) ThreadLocalRandom.current().nextInt(65, 90 + 1);
    }

    private void setMatrix() {
        /*
        * The generated matrix have a 'border' filled with zeros,
        * in order to provide a better way to verify the results across
        * the given letters.
        * */

        matrix = new char[6][6];

        //Static matrix for test
//        matrix[0] = new char[]{'0', '0', '0', '0', '0', '0'};
//        matrix[1] = new char[]{'0', 'E', 'A', 'E', 'P', '0'};
//        matrix[2] = new char[]{'0', 'X', 'R', 'R', 'N', '0'};
//        matrix[3] = new char[]{'0', 'S', 'A', 'M', 'A', '0'};
//        matrix[4] = new char[]{'0', 'A', 'L', 'D', 'M', '0'};
//        matrix[5] = new char[]{'0', '0', '0', '0', '0', '0'};

        //Randomically generating matrix
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                matrix[i][j] = '0';
            }
        }

        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                matrix[i][j] = generateRandomLetter();
            }
        }
    }

    private String printMatrixSocket(Socket Client) throws IOException {
        /*
        * Printing the matrix
        * */
        String line = " ";
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                line += Character.toString(matrix[i][j]) + " ";
            }
            sentMsg(Client, line + "\n");
            line = " ";
        }
        return null;
    }



    private void printMatrix() {
        /*
        * Printing the matrix
        * */
        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                System.out.print(Character.toString(matrix[i][j]) + ' ');
            }
            System.out.println("");
        }
    }

    private boolean checkWord(String word) {
        /*
        * This method check if the word is valid or not.
        * TODO: check if the word is a valid word in Portuguese language
        * */

        HashMap<Boolean, Boolean> results = new HashMap<Boolean, Boolean>();

        //Execute a loop in every matrix positions calling checkLetter method
        for (int x = 1; x < 5; x++) {
            for (int y = 1; y < 5; y++) {
                /*
                *  A Hash is created to store two possible keys: true and false.
                *  If at the end of this loop, the hash contains only false, the word wasn't found.
                *  But if exists two keys, the result is true.
                *
                *  ## Attributes##
                *  word = Word to validate
                *  x, y = Matrix position of the letter
                * */
                boolean result = checkLetter(word.toUpperCase().toCharArray(), x, y, word.length());
                results.put(result, result);
            }
        }

        return results.keySet().size() > 1;
    }



    private boolean checkLetter(char[] letters, int x, int y, int size) {
        /*
        * A recursive method that validates each letter in the word.
        * For each recursive call, the size of the word char array is verified and also
        * the current letter in compare to letters in matrix
        * */
        if (letters.length > 0 && letters[0] == matrix[x][y]) {

            /*
            * letters.length == 1 means that is the last letter in the word char array.
            * This also means: It's time to scape and the word was founded. :)
            * */
            if (letters.length == 1) {
                return true;
            }


            char[] array = {};

            /*
            * As part of the recursive call, we delete the letter in position 0 in the char array
            * of characters. Each time a letter is founded, we remove a letter from array.
            * */
            if (letters.length > 0)
                array = Arrays.copyOfRange(letters, 1, letters.length);

            /*
            * Here we are calling this same method recursivelly to check each neighbohood letter
            * in the matrix in order to follow the rule of the game Parole that says the next choosen letter is
            * only valid if it's in the side of the previous letter at the matrix.
            * */
            return checkLetter(array, x - 1, y - 1, size) ||
                    checkLetter(array, x, y - 1, size) ||
                    checkLetter(array, x + 1, y - 1, size) ||
                    checkLetter(array, x - 1, y, size) ||
                    checkLetter(array, x + 1, y, size) ||
                    checkLetter(array, x - 1, y + 1, size) ||
                    checkLetter(array, x, y + 1, size) ||
                    checkLetter(array, x + 1, y + 1, size);
        } else {
            if (letters.length == 0 || (letters[0] != matrix[x][y] && letters.length != size))
                return false;
        }
        return false;
    }

    //private String CheckPT(String word){

    //}

    private void readFromKeyboard() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entre com a palavra: ");
        String word = scanner.next();
        do {
            System.out.println(checkWord(word));
            System.out.println("Entre com outra palavra palavra: ");
            word = scanner.next();
        } while (!word.equalsIgnoreCase("fim"));

    }

    public void start() {
        /*
        *
        * Public method that start the game
        *
        * */
        String strMsgClient;

        long time = 10000;

        try {
            // Making a Socket with the server
            ServerSocket socketServer = new ServerSocket(6789);
            System.out.println("Servidor Ativo!");
            System.out.println("---------------------------");
            System.out.println("Aguardando conexoes na porta " + 6789);
            while(true) {
                // Accepting connection
                Socket connection = socketServer.accept();

                int score = 0;

                // Return client ip adress
                System.out.println("Nova conexão com o cliente " +
                        connection.getInetAddress().getHostAddress());

                setMatrix();
                printMatrixSocket(connection);

                //Game Start
                long time0 = System.currentTimeMillis();
                sentMsg(connection, "\nEntre com a palavra: \n");
                strMsgClient = readMsg(connection);
                checkWord(strMsgClient);

                //Jogo rodando por 10 segundos
                while ((System.currentTimeMillis() - time0) < time) {
                    if(checkWord(strMsgClient)==true) {
                        score+=strMsgClient.length();
                        sentMsg(connection, "\npalavra correta!\n\nVoce ganhou "
                                + strMsgClient.length() + " pontos!\n" + "Pontuacao atual: "
                                + score +" pontos\n\n");
                    }
                    else
                        sentMsg(connection, "palavra errada!\n");

                    printMatrixSocket(connection);
                    sentMsg(connection, "\nEntre com outra palavra: \n");
                    strMsgClient=readMsg(connection);
                }
                sentMsg(connection, "\nfim do jogo, sua pontuacao: " + score +"\n\n");
                break;

            }
        } catch (IOException e) {
            System.out.println("Erro: " + e);
            System.exit(0);
        }

        //setMatrix();
        //printMatrix();
        //readFromKeyboard();


    }

        //Reading message
    public String readMsg(Socket clientSocket) throws IOException {
        BufferedReader msg = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        return msg.readLine();
    }
        //Sending message
    public void sentMsg(Socket connect, int msg) throws IOException {
        DataOutputStream msgParaCliente = new DataOutputStream(connect.getOutputStream());
        msgParaCliente.writeBytes(valueOf(msg));
    }

    public void sentMsg(Socket connect, String msg) throws IOException {
        DataOutputStream msgParaCliente = new DataOutputStream(connect.getOutputStream());
        msgParaCliente.writeBytes(msg);
    }

}
