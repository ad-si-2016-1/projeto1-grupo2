package com.ufg.projeto1grupo2;


import java.net.ServerSocket;

public class Main {

    public static void main(String[] args)  throws Exception {

        ServerSocket listener = new ServerSocket(6543);
        Logic logic = new Logic();
        logic.start();

        System.out.println("Servidor ativo:");
        try {
            while (true) {
                Game game = new Game();
                //Inicia a thread com a lógica do jogo
                Game.Player player1 = game.new Player(listener.accept(), "1", logic);
                System.out.println("Jogador 1 conectado");
                Game.Player player2 = game.new Player(listener.accept(), "2", logic);
                System.out.println("Jogador 2 conectado");
                //Inicia a thread, executa método public void run()
                player1.start();
                player2.start();

            }
        } finally {
            listener.close();
        }

    }

}
