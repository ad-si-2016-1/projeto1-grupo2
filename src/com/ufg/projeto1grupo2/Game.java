package com.ufg.projeto1grupo2;

import java.io.*;
import java.net.Socket;


/**
 * Created by julioalfredo on 5/17/16.
 */
class Game{

    class Player extends Thread{

        String name;
        Socket socket;
        BufferedReader input;
        PrintWriter output;
        Logic logic;
        Integer result = 0;


        public Player(Socket socket, String player, Logic logic) {
            this.socket = socket;
            this.name = player;
            try {
                input = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);

                if (player == "1") {
                    output.println("0");
                    output.println("1");
                }else if (player == "2"){
                    output.println("0");
                }

                this.logic = logic;
                // output.println("MENSAGEM: Aguardando oponente se conectar");

            } catch (IOException e) {
                System.out.println("Jogador morreu: " + e);
            }
        }

        public Socket getSocket() {
            return socket;
        }

        public void run() {
            try {

                //Seta o tempo de jogo
                long time = 10000;
                long time0 = System.currentTimeMillis();

                //A thread começa apenas após todos jogadores conectarem
                output.println("2,"+logic.getMatrix());

                // Aceita repetidamente comandos do cliente e os processa
                while ((System.currentTimeMillis() - time0) < time) {
                    String word = input.readLine();
                    //Soma valor em result
                    if (logic.checkWord(word)){
                        result++;
                    }
                    //Envia resultado da verificação da palavra para o cliente
                    output.println("3,"+logic.checkWord(word)+","+result);
                    //Envia matriz novamente e pede para que o cliente entre com outra palavra
                    output.println("4,"+logic.getMatrix());
                }
                output.println("5,"+result);
                String word = input.readLine();
                if (word.equals("f")){
                    socket.close();
                }


            } catch (IOException e) {
                System.out.println("Jogador morreu: " + e);
            } finally {
                try {socket.close();} catch (IOException e) {}
            }
        }
    }

}