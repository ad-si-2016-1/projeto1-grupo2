package com.ufg.projeto1grupo2.clients.java;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.util.Scanner;



public class ClientJava {
    public static void main(String args[]) throws Exception{
        String strMsg = null;
        String strMsgServer = null;

        // Criando socket para o servidor
        Scanner scanner = new Scanner(System.in);
        Socket socketClient = new Socket("192.168.0.13",6543);

        //Buffer para receber msg do servidor
        BufferedReader msgfromServer = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
        // Criando stream para enviar mensagem para o servidor
        DataOutputStream msgToServer = new DataOutputStream(socketClient.getOutputStream());

        do{
            // Recebendo mensagem do servidor
            strMsgServer = msgfromServer.readLine();
            // Decidindo o que fazer
            String[] parts = strMsgServer.split(",");
            switch (parts[0]){
                case "0":
                    System.out.println("Você está conectado!");
                    break;
                case "1":
                    System.out.println("Aguardando próximo jogador...");
                    break;
                //Mostra instruções para o jogo e matriz
                case "2":
                    System.out.println("O jogo começou!");
                    System.out.println("## A partir de agora, você tem 3 minutos para enviar suas respostas ##");
                    System.out.println("## Encontre a maior quantidade de palavras na matriz abaixo: ##");
                    char[] charMatrixArray = parts[1].toCharArray();
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray, 0, 3)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray, 4, 7)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray, 8, 11)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray, 12, 15)));

                    System.out.println("");
                    System.out.println("## Entre com uma palavra: ##");
                    strMsg = scanner.next();
                    // Enviando mensagem digitada ao servidor
                    msgToServer.writeBytes(strMsg + "\n");
                    break;
                //Mostra resultado da verificação
                case "3":
                    if(Boolean.valueOf(parts[1])){
                        System.out.println("Você acertou!");
                        System.out.println("Você tem "+parts[2]+" pontos até o momento");
                    }else{
                        System.out.println("Você errou!");
                        System.out.println("Você tem "+parts[2]+" pontos até o momento");
                    }
                    break;
                //Mostra matriz cada vez que o usuário entra com uma palavra
                case "4":
                    char[] charMatrixArray1 = parts[1].toCharArray();
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray1, 0, 3)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray1, 4, 7)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray1, 8, 11)));
                    System.out.println(Arrays.toString(Arrays.copyOfRange(charMatrixArray1, 12, 15)));
                    System.out.println("");
                    System.out.println("## Entre com outra palavra: ##");
                    strMsg = scanner.next();
                    // Enviando mensagem digitada ao servidor
                    msgToServer.writeBytes(strMsg + "\n");
                    break;
                case "5":
                    System.out.println("O jogo terminou!");
                    System.out.println("Pressione 'f' para ver o placar...");
                    strMsg = scanner.next();
                    msgToServer.writeBytes(strMsg + "\n");
                    break;
            }

        }while(socketClient.isConnected());

   }
}