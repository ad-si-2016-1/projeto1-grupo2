require 'socket'
require 'awesome_print'

sock = TCPSocket.new '127.0.0.1', 6543


while line = sock.gets
  resp = line.delete!("\n").split(',')
  case resp[0]
    when "0"
      ap "Você está conectado."
    when "1"
      ap "Aguardando próximo jogador..."
    when "2"
      ap "O jogo começou!"
      ap "## A partir de agora, você tem 3 minutos para enviar suas respostas ##"
      ap "## Encontre a maior quantidade de palavras na matriz abaixo: ##"
      ap resp[1][0..3].scan(/.{1,1}/).join(' ')
      ap resp[1][4..7].scan(/.{1,1}/).join(' ')
      ap resp[1][8..11].scan(/.{1,1}/).join(' ')
      ap resp[1][12..15].scan(/.{1,1}/).join(' ')
      ap "Escreva uma palavra: "
      word = gets.chomp
      sock.send(word+"\n", 0)
      sock.flush
    when "3"
      if resp[1] == "true"
        ap "Você acertou!"
        ap "Você tem #{resp[2]} até o momento"
        ap "Escreva outra palavra: "
        word = gets.chomp
        sock.send(word+"\n", 0)
        sock.flush
      else
        ap "Você errou!"
        ap "Você tem #{resp[2]} até o momento"
        ap "Escreva outra palavra: "
        word = gets.chomp
        sock.send(word+"\n", 0)
        sock.flush
      end
    when "5"
      ap "O jogo terminou!"
      ap "Digite 'f' para ver o placar..."
      word = gets.chomp
      sock.send(word+"\n", 0)
      sock.flush
  else
    "Ocorreu um problema!"
  end
end
sock.close
